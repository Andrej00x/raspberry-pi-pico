// Count from 0 to 9999 with increments every 100 ms
// This display has a common cathode

#include "pico/stdlib.h"

const uint delay = 5; // how long (in ms) should a specific display be turned on

// segments A, B, ..., G, DP
const uint display_pins [8] = {
    6, 7, 8, 9, 10, 11, 12, 13
};

// displays D1 (rightmost), D2, D3, D4 (leftmost)
const uint selector_pins [4] = {
    14, 15, 16, 17
};

int bits[10] = {
        0x3f,  // 0
        0x06,  // 1
        0x5b,  // 2
        0x4f,  // 3
        0x66,  // 4
        0x6d,  // 5
        0x7d,  // 6
        0x07,  // 7
        0x7f,  // 8
        0x6f,  // 9
};

int main() {

    // initialize 7-segment display
    for (int i = 0; i < 8; i++) {
        gpio_init(display_pins[i]);
        gpio_set_dir(display_pins[i], GPIO_OUT);
        // gpio_set_outover(display_pins[i], GPIO_OVERRIDE_INVERT); // UNCOMMENT IF COMMON ANODE
    }

    for (int i = 0; i < 4; i++) {
        gpio_init(selector_pins[i]);
        gpio_set_dir(selector_pins[i], GPIO_OUT);
        gpio_set_outover(selector_pins[i], GPIO_OVERRIDE_INVERT);
    }

    while (true) {
        for (int thousands = 0; thousands < 10; thousands++) {
            for (int hundereds = 0; hundereds < 10; hundereds++) {
                for (int tens = 0; tens < 10; tens++) {
                    for (int ones = 0; ones < 10; ones++) {

                        // display multiplex (value is incremented every 5*4*delay ms)
                        for (int i = 0; i < 5; i++) {
                                
                            // We are starting with GPIO 6, our bitmap starts at bit 0 so shift to start at 6.
                            int32_t mask = bits[ones] << display_pins[0];
                            gpio_put(selector_pins[0], true);
                            gpio_set_mask(mask); // set all GPIOs, selected by mask, to high
                            sleep_ms(delay);
                            gpio_clr_mask(mask); // set all GPIOs, selected by mask, to low
                            gpio_put(selector_pins[0], false);

                            mask = bits[tens] << display_pins[0];
                            gpio_put(selector_pins[1], true);
                            gpio_set_mask(mask);
                            sleep_ms(delay);
                            gpio_clr_mask(mask);
                            gpio_put(selector_pins[1], false);

                            mask = bits[hundereds] << display_pins[0];
                            gpio_put(selector_pins[2], true);
                            gpio_set_mask(mask);
                            sleep_ms(delay);
                            gpio_clr_mask(mask);
                            gpio_put(selector_pins[2], false);

                            mask = bits[thousands] << display_pins[0];
                            gpio_put(selector_pins[3], true);
                            gpio_set_mask(mask);
                            sleep_ms(delay);
                            gpio_clr_mask(mask);
                            gpio_put(selector_pins[3], false);
                        }
                    }
                }
            }
        }
    }

    return 0;
}