cmake_minimum_required(VERSION 3.12)

include(pico_sdk_import.cmake)

set(CMAKE_C_STANDARD 11)
set(CMAKE_CXX_STANDARD 17)

pico_sdk_init()

project(4x7-segment)

add_executable(4x7-segment 4x7-segment.c)

target_link_libraries(4x7-segment
                      pico_stdlib
)

pico_add_extra_outputs(4x7-segment)