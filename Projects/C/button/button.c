// change rgb LED colour with a press of a button
// there's a 200 ms delay between button presses (debouncing)

#include "pico/stdlib.h"
#include "hardware/pwm.h"

const uint RED_PIN = 0;
const uint GREEN_PIN = 1;
const uint BLUE_PIN = 2;
const uint BUTTON_PIN = 3;
const uint16_t CYCLE = 255;
uint32_t time; // used for debouncing

void rgb_set_colour(uint16_t red_value, uint16_t green_value, uint16_t blue_value);

void change_colour();

int main() {
    time = to_ms_since_boot(get_absolute_time());

    // set up an rgb led
    gpio_set_function(RED_PIN, GPIO_FUNC_PWM);
    gpio_set_function(GREEN_PIN, GPIO_FUNC_PWM);
    gpio_set_function(BLUE_PIN, GPIO_FUNC_PWM);

    // set up a button
    gpio_pull_up(BUTTON_PIN);
    gpio_set_irq_enabled_with_callback(BUTTON_PIN, GPIO_IRQ_EDGE_FALL, true, &change_colour);

    rgb_set_colour(255, 255, 255);

    while(true) {
        // infinite loop
    }

    return 0;
}

void change_colour() {
    static uint stage = 0;
    const uint32_t delay = 200; // delay in ms for debouncing

    if ((to_ms_since_boot(get_absolute_time()) - time) > delay) {
        time = to_ms_since_boot(get_absolute_time());
        switch(stage) {
            case 0: rgb_set_colour(0, 0, 0);
                break;
            case 1: rgb_set_colour(127, 0, 0); // red
                break;
            case 2: rgb_set_colour(0, 127, 0); // greeen
                break;
            case 3: rgb_set_colour(0, 0, 127); // blue
                break;
            case 4: rgb_set_colour(127, 127, 0); // yellow
                break;
            case 5: rgb_set_colour(127, 0, 127); // magenta
                break;
            case 6: rgb_set_colour(0, 127, 127); // cyan
                break;
            case 7: rgb_set_colour(127, 127, 127); // white
                break;
            default: rgb_set_colour(127, 127, 127);
        }

        if(++stage > 7) stage = 0;
    }
}

// set RGB value of an LED
void rgb_set_colour(uint16_t red_value, uint16_t green_value, uint16_t blue_value) {
    uint slice_red = pwm_gpio_to_slice_num(RED_PIN);
    uint slice_green = pwm_gpio_to_slice_num(GREEN_PIN);
    uint slice_blue = pwm_gpio_to_slice_num(BLUE_PIN);

    pwm_set_wrap(slice_red, CYCLE);
    pwm_set_wrap(slice_green, CYCLE);
    pwm_set_wrap(slice_blue, CYCLE);

    if(red_value > CYCLE) red_value = CYCLE;
    if(green_value > CYCLE) green_value = CYCLE;
    if(blue_value > CYCLE) blue_value = CYCLE;

    // 0 means higher value (common anode => negative logic)
    pwm_set_chan_level(slice_red, pwm_gpio_to_channel(RED_PIN), (CYCLE - red_value)); //red
    pwm_set_chan_level(slice_green, pwm_gpio_to_channel(GREEN_PIN), (CYCLE - green_value)); // green
    pwm_set_chan_level(slice_blue, pwm_gpio_to_channel(BLUE_PIN), (CYCLE - blue_value)); // blue

    pwm_set_enabled(slice_red, true);
    pwm_set_enabled(slice_green, true);
    pwm_set_enabled(slice_blue, true);
}