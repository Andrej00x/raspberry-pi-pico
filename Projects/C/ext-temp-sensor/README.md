# External temperature and humidity sensor

Using the DHT11 sensor, measure and print humidity and temperature every two seconds (should also work with DHT22).
Note that period of two seconds is minimum for DHT to work correctly.