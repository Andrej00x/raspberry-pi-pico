#include <stdio.h>
#include "pico/stdlib.h"

typedef struct {
    float humidity;
    float temp_celsius;
} dht_reading;

float celsius_to_fahrenheit(float temperature);
void read_from_dht(dht_reading *result);
float int_to_float(int whole, int decimal);

const uint DHT_PIN = 16;
const uint MAX_TIMINGS = 85;
const uint MEASURE_PERIOD_MS = 2000; // this must be at least 2000 to get an accurate reading

// measure temperature and humidity every 2 seconds
int main() {
    stdio_init_all();
    gpio_init(DHT_PIN);

    while (true) {
        dht_reading reading;
        read_from_dht(&reading);
        printf("Humidity = %.1f%%, Temperature = %.1f C (%.1f F)\n",
               reading.humidity, reading.temp_celsius, celsius_to_fahrenheit(reading.temp_celsius));

        sleep_ms(MEASURE_PERIOD_MS);
    }

    return 0;
}

void read_from_dht(dht_reading *result) {
    int data[5] = {0, 0, 0, 0, 0};
    uint last = 1;
    uint j = 0;

    // send a 0 to DHT to begin transmission
    gpio_set_dir(DHT_PIN, GPIO_OUT);
    gpio_put(DHT_PIN, 0);
    sleep_ms(20);
    gpio_set_dir(DHT_PIN, GPIO_IN);

    for (uint i = 0; i < MAX_TIMINGS; i++) {
        uint count = 0;
        while (gpio_get(DHT_PIN) == last) {
            count++;
            busy_wait_us_32(1);
            if (count == 255) break;
        }
        last = gpio_get(DHT_PIN);
        if (count == 255) break;

        if ((i >= 4) && (i % 2 == 0)) {
            data[j / 8] <<= 1;
            if (count > 50) data[j / 8] |= 1; // if you get "Bad data", try changing 50 to some other value
            j++;
        }
    }

    if ((j >= 40) && (data[4] == ((data[0] + data[1] + data[2] + data[3]) & 0xFF))) {
        result->humidity = int_to_float(data[0], data[1]);
        result->temp_celsius = int_to_float(data[2], data[3]);
    } else {
        printf("Bad data\n");
    }
}

// return a float from two integers representing whole and decimal parts
// if decimal part is negative, it will be converted to positive
float int_to_float(int whole, int decimal) {
    float result = 0;

    if(decimal < 0) {
        decimal = -decimal;
    }

    while(decimal > 0) {
        result += decimal % 10;
        decimal /= 10;
        result /= 10;
    }

    return result + whole;
}

float celsius_to_fahrenheit(float temperature) {
    return (temperature * 9 / 5 + 32);
}