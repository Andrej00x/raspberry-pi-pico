# Internal temperature sensor

Using the internal temperature sensor, collect 1000 temperature measurements and then print out the average.
Repeat this process every second (using a timer). Toggle the integrated LED for every sent result.