#include <stdio.h>
#include "pico/stdlib.h"
#include "hardware/adc.h"

bool repeating_measurement(repeating_timer_t *rt);
float celsius_to_fahrenheit(float temperature);

// Collect 1000 temperature samples and print the average
// wait for 1 second before measuring again
// also toggle led for every sent result
int main() {
    const uint LED_PIN = PICO_DEFAULT_LED_PIN;
    const uint MEASURING_PERIOD_MS = 1000;
    
    stdio_init_all();

    // initialize internal temperature sensor
    adc_init();
    adc_set_temp_sensor_enabled(true);
    adc_select_input(4); // temperature sensor is connected to the 5th adc channel

    repeating_timer_t timer;
    // negative timeout means exact delay (rather than delay between callbacks)
    if(!add_repeating_timer_ms(-MEASURING_PERIOD_MS, repeating_measurement, NULL, &timer)) {
        printf("Failed to add timer.\n");
        return 1;
    }

    // inititalize the led
    gpio_init(LED_PIN);
    gpio_set_dir(LED_PIN, GPIO_OUT);
    gpio_put(LED_PIN, true);

    while (true) {
        // infinite loop
    }

    return 0;
}

bool repeating_measurement(repeating_timer_t *rt) {
    const uint SAMPLES_LIMIT = 1000;
    const uint LED_PIN = PICO_DEFAULT_LED_PIN;
    uint samples = 0;
    float average_temperature = 0;

    do{
        samples++;
        uint16_t raw = adc_read();
        const float conversion = 3.3f / (1<<12); // 12-bit conversion, 3.3 is Vref
        float voltage = raw * conversion;
        float temperature = 27 - (voltage - 0.706) / 0.001721; // formula from datasheet
        average_temperature += temperature;
    } while(samples < SAMPLES_LIMIT);

    gpio_put(LED_PIN, !gpio_get(LED_PIN)); // toggle led

    average_temperature /= SAMPLES_LIMIT;
    printf("Temperature: %.2f C\t%.2f F\n", average_temperature, celsius_to_fahrenheit(average_temperature));
    
    return true;
}

float celsius_to_fahrenheit(float temperature) {
    return (1.8 * temperature + 32);
}