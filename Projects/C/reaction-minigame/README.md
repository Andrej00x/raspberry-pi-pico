# Reaction time minigame

Press the button as fast as you can after the LED changes colour.
LED lights meanings:
* BLUE - Waiting a button press to start the counter
* RED - Countdown has started
* GREEN - Countdown has finished, press the button to stop the counter
* NONE - Displaying reaction time

If you do not press the button after 100 seconds, the display will print all nines, unless you wait around
50 days (then the integer overflow will occur).