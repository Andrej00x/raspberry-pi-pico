// Press the button after LED changes colour
// Reaction time will be displayed on a 4x7-segment display

#include <stdlib.h>
#include "pico/stdlib.h"
#include "rgb_led.h"

enum states {STANDBY, COUNTDOWN, FINISHED, RESULT};

rgb_led led;

const uint BUTTON_PIN = 3;

// limits (in ms) for generating random delay
const int upper = 6000;
const int lower = 2000;

uint32_t time; // used for debouncing
uint32_t start_time;
int counter;

volatile enum states state = STANDBY;

const uint delay = 5; // how long (in ms) should a specific display be turned on

int64_t finished_counting(alarm_id_t id, void *user_data);
void button_callback();
void print_to_display(uint32_t number);

// segments A, B, ..., G, DP
const uint display_pins [8] = {
    6, 7, 8, 9, 10, 11, 12, 13
};

// displays D1, D2, D3, D4
const uint selector_pins [4] = {
    14, 15, 16, 17
};

int bits[11] = {
        0x3f,  // 0
        0x06,  // 1
        0x5b,  // 2
        0x4f,  // 3
        0x66,  // 4
        0x6d,  // 5
        0x7d,  // 6
        0x07,  // 7
        0x7f,  // 8
        0x6f,  // 9
        0x80   // DP
};

int main() {
    repeating_timer_t timer;

    led.RED_PIN = 0;
    led.GREEN_PIN = 1;
    led.BLUE_PIN = 2;

    gpio_set_function(led.RED_PIN, GPIO_FUNC_PWM);
    gpio_set_function(led.GREEN_PIN, GPIO_FUNC_PWM);
    gpio_set_function(led.BLUE_PIN, GPIO_FUNC_PWM);

    gpio_pull_up(BUTTON_PIN);
    gpio_set_irq_enabled_with_callback(BUTTON_PIN, GPIO_IRQ_EDGE_FALL, true, &button_callback);

    for (int i = 0; i < 8; i++) {
        gpio_init(display_pins[i]);
        gpio_set_dir(display_pins[i], GPIO_OUT);
    }
    for (int i = 0; i < 4; i++) {
        gpio_init(selector_pins[i]);
        gpio_set_dir(selector_pins[i], GPIO_OUT);
        gpio_set_outover(selector_pins[i], GPIO_OVERRIDE_INVERT);
    }

    rgb_set_colour(0, 0, 127, led);

    while(true) {
        if (state == RESULT) print_to_display(time - (start_time + counter));
        else print_to_display(0);
    }

    return 0;
}

void button_callback() {
    const uint32_t debounce = 150; // delay in ms for debouncing

    if ((to_ms_since_boot(get_absolute_time()) - time) > debounce) {
        time = to_ms_since_boot(get_absolute_time());
        switch(state) {

            case STANDBY:
                srand(to_ms_since_boot(get_absolute_time()));
                counter = (int)(((float) rand() / RAND_MAX) * (upper - lower) + lower);
                rgb_set_colour(127, 0, 0, led);
                add_alarm_in_ms(counter, finished_counting, NULL, false);
                start_time = to_ms_since_boot(get_absolute_time());
                state = COUNTDOWN;
                break;

            case FINISHED:
                rgb_set_colour(0, 0, 0, led);
                state = RESULT;
                break;

            case RESULT:
                state = STANDBY;
                rgb_set_colour(0, 0, 127, led);
                break;

            default: break;
        }
    }
}

int64_t finished_counting(alarm_id_t id, void *user_data) {
    state = FINISHED;
    rgb_set_colour(0, 127, 0, led);

    return 0;
}

void print_to_display(uint32_t number) {
    for (int i = 0; i < 4; i++) gpio_put(selector_pins[i], false);

    if (number == 0) {
        gpio_set_mask(bits[10] << display_pins[0]);
        for(int i = 0; i < 4; i++) gpio_put(selector_pins[i], true);
        return;
    }
    if (number == -1) {
        gpio_set_mask(bits[9] << display_pins[0]);
        for(int i = 0; i < 4; i++) gpio_put(selector_pins[i], true);
        return;
    }

    int32_t mask[4];

    if (number < 10) {
        mask[3] = (bits[0] | bits[10]) << display_pins[0];
        mask[2] = mask[1] = bits[0];
        mask[0] = bits[number];
    }

    if (number < 100) {
        mask[3] = (bits[0] | bits[10]) << display_pins[0];
        mask[2] = bits[0] << display_pins[0];
        mask[1] = bits[number / 10] << display_pins[0];
        mask[0] = bits[number % 10] << display_pins[0];
    }
    else if (number < 1000) {
        mask[3] = (bits[0] | bits[10]) << display_pins[0];
        mask[2] = bits[number / 100] << display_pins[0];
        mask[1] = bits[(number % 100) / 10] << display_pins[0];
        mask[0] = bits[number % 10] << display_pins[0];
    }
    else if (number < 10000) {
        mask[3] = (bits[number / 1000] | bits[10]) << display_pins[0];
        mask[2] = bits[(number % 1000) / 100] << display_pins[0];
        mask[1] = bits[(number % 100) / 10] << display_pins[0];
        mask[0] = bits[number % 10] << display_pins[0];
    }
    else if (number < 100000) {
        mask[3] = bits[number / 10000] << display_pins[0];
        mask[2] = (bits[(number % 10000) / 1000] | bits[10]) << display_pins[0];
        mask[1] = bits[(number % 1000) / 100] << display_pins[0];
        mask[0] = bits[(number % 100) / 10] << display_pins[0];
    } else {
        print_to_display(-1); // number is too large
        return;
    }

    for (int i = 0; i < 5; i++) {
        for (int display = 0; display < 4; display++) {
            gpio_put(selector_pins[display], true);
            gpio_set_mask(mask[display]); // set all GPIOs, selected by mask, to high
            sleep_ms(delay);
            gpio_clr_mask(mask[display]); // set all GPIOs, selected by mask, to low
            gpio_put(selector_pins[display], false);
        }
    }
}