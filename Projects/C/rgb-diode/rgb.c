// RGB LED with common anode
// turn rgb led on to the selected colour

#include "hardware/pwm.h"
#include "pico/stdlib.h"

const uint RED_PIN = 0;
const uint GREEN_PIN = 1;
const uint BLUE_PIN = 2;
const uint16_t CYCLE = 255;

void rgb_set_colour(uint16_t red_value, uint16_t green_value, uint16_t blue_value);

int main() {
    gpio_set_function(RED_PIN, GPIO_FUNC_PWM);
    gpio_set_function(GREEN_PIN, GPIO_FUNC_PWM);
    gpio_set_function(BLUE_PIN, GPIO_FUNC_PWM);

    rgb_set_colour(127, 127, 127);

    return 0;
}

// set RGB value of an LED
void rgb_set_colour(uint16_t red_value, uint16_t green_value, uint16_t blue_value) {
    uint slice_red = pwm_gpio_to_slice_num(RED_PIN);
    uint slice_green = pwm_gpio_to_slice_num(GREEN_PIN);
    uint slice_blue = pwm_gpio_to_slice_num(BLUE_PIN);

    pwm_set_wrap(slice_red, CYCLE);
    pwm_set_wrap(slice_green, CYCLE);
    pwm_set_wrap(slice_blue, CYCLE);

    if(red_value > CYCLE) red_value = CYCLE;
    if(green_value > CYCLE) green_value = CYCLE;
    if(blue_value > CYCLE) blue_value = CYCLE;

    // 0 means higher value (common anode => negative logic)
    pwm_set_chan_level(slice_red, pwm_gpio_to_channel(RED_PIN), (CYCLE - red_value)); //red
    pwm_set_chan_level(slice_green, pwm_gpio_to_channel(GREEN_PIN), (CYCLE - green_value)); // green
    pwm_set_chan_level(slice_blue, pwm_gpio_to_channel(BLUE_PIN), (CYCLE - blue_value)); // blue

    pwm_set_enabled(slice_red, true);
    pwm_set_enabled(slice_green, true);
    pwm_set_enabled(slice_blue, true);
}