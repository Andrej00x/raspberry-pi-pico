# Snake on 4x7-segment display

Rotate a snake (segment D) between the displays from left to right. When a button is pressed, make the snake jump 
(on next display, in order light segments E > F > A > B > C).