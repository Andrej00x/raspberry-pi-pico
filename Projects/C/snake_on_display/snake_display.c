// Rotate segment D between 4 displays (left to right)
// When a button is pressed, on a next lit display light segments E>F>A>B>C

#include "pico/stdlib.h"

enum segment {A, B, C, D, E, F, G, DP};

const uint delay = 300; // how long (in ms) should a specific display be active
const uint BUTTON_PIN = 3;
bool jump_flag;

const uint display_pins [8] = {
    6, 7, 8, 9, 10, 11, 12, 13
};

const uint selector_pins [4] = {
    14, 15, 16, 17
};

const int segments[8] = {
        1,       // A
        1 << 1,  // B
        1 << 2,  // C
        1 << 3,  // D
        1 << 4,  // E
        1 << 5,  // F
        1 << 6,  // G
        1 << 7   // DP
};

void button_callback();
void jump(int* display);

int main() {
    jump_flag = false;

    // initialize 7-segment display
    for (int i = 0; i < 8; i++) {
        gpio_init(display_pins[i]);
        gpio_set_dir(display_pins[i], GPIO_OUT);
    }
    for (int i = 0; i < 4; i++) {
        gpio_init(selector_pins[i]);
        gpio_set_dir(selector_pins[i], GPIO_OUT);
        gpio_set_outover(selector_pins[i], GPIO_OVERRIDE_INVERT);
    }

    // set up a button
    gpio_pull_up(BUTTON_PIN);
    gpio_set_irq_enabled_with_callback(BUTTON_PIN, GPIO_IRQ_EDGE_FALL, true, &button_callback);

    while (true) {
        for(int display = 3; display >= 0; display--) {
            gpio_put(selector_pins[display], true);
            gpio_set_mask(segments[D] << display_pins[0]);

            sleep_ms(delay);

            gpio_put(selector_pins[display], false);

            if (jump_flag) jump(&display);
        }
    }

    return 0;
}

void button_callback() {

    //disable button interrupt
    gpio_set_irq_enabled_with_callback(BUTTON_PIN, GPIO_IRQ_EDGE_FALL, false, &button_callback);

    jump_flag = true;
}

void jump(int* display) {
    int sequence[5] = {
        segments[E],
        segments[F],
        segments[A],
        segments[B],
        segments[C]
    };

    if(--(*display) < 0) *display = 3; // switch display and check if it's out of bounds
    gpio_put(selector_pins[*display], true);
    gpio_clr_mask(segments[D] << display_pins[0]);
    
    for (int i = 0; i < 5; i++) {
        old_time = to_ms_since_boot(get_absolute_time());
        gpio_set_mask(sequence[i] << display_pins[0]);

        sleep_ms(delay);

        gpio_clr_mask(sequence[i] << display_pins[0]);
    }

    gpio_put(selector_pins[*display], false);
    jump_flag = false;

    //reenable button interrupt
    gpio_set_irq_enabled_with_callback(BUTTON_PIN, GPIO_IRQ_EDGE_FALL, true, &button_callback);
}