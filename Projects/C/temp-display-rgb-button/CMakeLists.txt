cmake_minimum_required(VERSION 3.12)

include(pico_sdk_import.cmake)

set(CMAKE_C_STANDARD 11)
set(CMAKE_CXX_STANDARD 17)

pico_sdk_init()

project(temp-display-rgb-button)
include_directories(RGB_LED DHT)
file(GLOB SOURCES "RGB_LED/*.c" "DHT/*.c")
add_executable(temp_display_rgb_button tempdisplayrgbbutton.c ${SOURCES})

target_link_libraries(temp_display_rgb_button
                      pico_stdlib
                      hardware_pwm
)

pico_add_extra_outputs(temp_display_rgb_button)