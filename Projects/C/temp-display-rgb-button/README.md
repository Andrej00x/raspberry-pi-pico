# External temperature and humidity sensor with RGB LED, button and 4x7-seg display

Every two seconds measure humidity and temperature with DHT11 sensor (should also work with DHT22).
Set the LED to specific colour based on the temperature.
Turn on integrated LED during DHT transmission.
Display temperature on 4x7-segment display. Cycle between celsius, fahrenheit and humidity with a press of a button.

*This project is most of my standalone projects combined.*