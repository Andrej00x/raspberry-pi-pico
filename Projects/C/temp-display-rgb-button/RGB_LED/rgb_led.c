#include "rgb_led.h"
#include "hardware/pwm.h"

const uint16_t CYCLE = 255;

// set RGB value of an LED
void rgb_set_colour(uint16_t red_value, uint16_t green_value, uint16_t blue_value, rgb_led led) {
    uint slice_red = pwm_gpio_to_slice_num(led.RED_PIN);
    uint slice_green = pwm_gpio_to_slice_num(led.GREEN_PIN);
    uint slice_blue = pwm_gpio_to_slice_num(led.BLUE_PIN);

    pwm_set_wrap(slice_red, CYCLE);
    pwm_set_wrap(slice_green, CYCLE);
    pwm_set_wrap(slice_blue, CYCLE);

    if(red_value > CYCLE) red_value = CYCLE;
    if(green_value > CYCLE) green_value = CYCLE;
    if(blue_value > CYCLE) blue_value = CYCLE;

    // 0 means higher value (common anode => negative logic)
    pwm_set_chan_level(slice_red, pwm_gpio_to_channel(led.RED_PIN), (CYCLE - red_value)); //red
    pwm_set_chan_level(slice_green, pwm_gpio_to_channel(led.GREEN_PIN), (CYCLE - green_value)); // green
    pwm_set_chan_level(slice_blue, pwm_gpio_to_channel(led.BLUE_PIN), (CYCLE - blue_value)); // blue

    pwm_set_enabled(slice_red, true);
    pwm_set_enabled(slice_green, true);
    pwm_set_enabled(slice_blue, true);
}