#include "pico/stdlib.h"

typedef struct {
    uint RED_PIN;
    uint GREEN_PIN;
    uint BLUE_PIN;
} rgb_led;

void rgb_set_colour(uint16_t red_value, uint16_t green_value, uint16_t blue_value, rgb_led led);