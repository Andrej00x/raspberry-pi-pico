#include <stdio.h>
#include "pico/stdlib.h"
#include "rgb_led.h"
#include "dht.h"

enum units {CELSIUS, FAHRENHEIT, HUMIDITY, END};
enum characters {C = 10, F, H, MINUS, DP}; // used for easier printing to display

float celsius_to_fahrenheit(float temperature);
void change_display();
void print_fahrenheit(float temp_f);
void print_humidity(float hum);
void print_celsius(float temp_c);

const uint delay = 5; // how long (in ms) should a specific display be turned on (shoudn't change this value)
const uint refresh_seconds = 2; // how often should display be refreshed and DHT measurement be done (if delay is not changed)

const uint DHT_PIN = 18;
const uint BUTTON_PIN = 3;
uint32_t time; // used for debouncing
enum units current_unit;

// segments A, B, ..., G, DP
const uint display_pins [8] = {
    6, 7, 8, 9, 10, 11, 12, 13
};

// displays D1, D2, D3, D4
const uint selector_pins [4] = {
    14, 15, 16, 17
};

int bits[15] = {
        0x3f,  // 0
        0x06,  // 1
        0x5b,  // 2
        0x4f,  // 3
        0x66,  // 4
        0x6d,  // 5
        0x7d,  // 6
        0x07,  // 7
        0x7f,  // 8
        0x6f,  // 9
        0x39,  // C
        0x71,  // F
        0x76,  // H
        0x40,  // -
        0x80   // DP
};

// measure temperature and humidity every 2 seconds
// set an RGB LED to specific colour depending on the temperature
// change desired display on a button press
int main() {
    rgb_led led;
    led.RED_PIN = 0;
    led.GREEN_PIN = 1;
    led.BLUE_PIN = 2;

    current_unit = CELSIUS;
    time = to_ms_since_boot(get_absolute_time());

    gpio_init(DHT_PIN);

    gpio_init(LED_PIN); // LED_PIN defined in dht.h
    gpio_set_dir(LED_PIN, GPIO_OUT);

    gpio_set_function(led.RED_PIN, GPIO_FUNC_PWM);
    gpio_set_function(led.GREEN_PIN, GPIO_FUNC_PWM);
    gpio_set_function(led.BLUE_PIN, GPIO_FUNC_PWM);

    gpio_pull_up(BUTTON_PIN);
    gpio_set_irq_enabled_with_callback(BUTTON_PIN, GPIO_IRQ_EDGE_FALL, true, &change_display);

    // initialize 7-segment display
    for (int i = 0; i < 8; i++) {
        gpio_init(display_pins[i]);
        gpio_set_dir(display_pins[i], GPIO_OUT);
        // gpio_set_outover(display_pins[i], GPIO_OVERRIDE_INVERT); // UNCOMMENT IF COMMON ANODE
    }
    for (int i = 0; i < 4; i++) {
        gpio_init(selector_pins[i]);
        gpio_set_dir(selector_pins[i], GPIO_OUT);
        // because selected display is active on LOW, invert our selected output
        gpio_set_outover(selector_pins[i], GPIO_OVERRIDE_INVERT);
    }

    while (true) {
        dht_reading reading;
        read_from_dht(&reading, DHT_PIN);

        // exact colour values may vary depending on your LED and resistors
        if (reading.temp_celsius <= 5) rgb_set_colour(0, 0, 127, led); // blue
        else if (reading.temp_celsius <= 15) rgb_set_colour(0, 127, 127, led); // cyan
        else if (reading.temp_celsius <= 25) rgb_set_colour(0, 127, 0, led); // green
        else if (reading.temp_celsius <= 30) rgb_set_colour(127, 127, 0, led); // yellow
        else rgb_set_colour(127, 0, 0, led); // red

        // Value display MUST be at least 2 seconds long (to get an accurate DHT reading)
        // Set the timings in print_x functions
        // period = delay*4*refresh_seconds*50
        switch(current_unit) {
            case FAHRENHEIT:
                print_fahrenheit(celsius_to_fahrenheit(reading.temp_celsius));
                break;
            case HUMIDITY:
                print_humidity(reading.humidity);
                break;
            case CELSIUS:
            default:
                print_celsius(reading.temp_celsius);
                break;
        }
    }

    return 0;
}

void change_display() {
    const uint32_t delay = 200; // delay in ms for debouncing

    if ((to_ms_since_boot(get_absolute_time()) - time) > delay) {
        time = to_ms_since_boot(get_absolute_time());

        if (++current_unit == END) current_unit = CELSIUS;
    }
}

float celsius_to_fahrenheit(float temperature) {
    return (temperature * 9 / 5 + 32);
}

void print_celsius(float temp_c) {
    int32_t mask[4];
    mask[0] = bits[C] << display_pins[0];

    if (temp_c <= -10) {
        mask[3] = bits[MINUS] << display_pins[0];
        mask[2] = bits[(int)(-temp_c) / 10] << display_pins[0];
        mask[1] = bits[(int)(-temp_c) % 10] << display_pins[0];
    }
    else if (temp_c < 0) {
        mask[3] = bits[MINUS] << display_pins[0];
        mask[2] = (bits[(int)(-temp_c)] | bits[DP]) << display_pins[0];
        mask[1] = bits[(int)((-temp_c) * 10) % 10] << display_pins[0];
    }
    else if (temp_c < 10) {
        mask[3] = (bits[(int) temp_c] | bits[DP]) << display_pins[0];
        mask[2] = bits[(int)(temp_c * 10) % 10] << display_pins[0];
        mask[1] = 0;
    }
    else {
        mask[3] = bits[(int)(temp_c / 10)] << display_pins[0];
        mask[2] = (bits[(int)temp_c % 10] | bits[DP]) << display_pins[0];
        mask[1] = bits[(int)(temp_c * 10) % 10] << display_pins[0];
    }

    for (int i = 0; i < 50 * refresh_seconds; i++) {
        for (int display = 0; display < 4; display++) {
            gpio_put(selector_pins[display], true);
            gpio_set_mask(mask[display]); // set all GPIOs, selected by mask, to high
            sleep_ms(delay);
            gpio_clr_mask(mask[display]); // set all GPIOs, selected by mask, to low
            gpio_put(selector_pins[display], false);
        }
    }
}

void print_fahrenheit(float temp_f) {
    int32_t mask[4];
    mask[0] = bits[F] << display_pins[0];

    if (temp_f <= -10) {
        mask[3] = bits[MINUS] << display_pins[0];
        mask[2] = bits[(int)(-temp_f) / 10] << display_pins[0];
        mask[1] = bits[(int)(-temp_f) % 10] << display_pins[0];
    }
    else if (temp_f < 0) {
        mask[3] = bits[MINUS] << display_pins[0];
        mask[2] = (bits[(int)(-temp_f)] | bits[DP]) << display_pins[0];
        mask[1] = bits[(int)((-temp_f) * 10) % 10] << display_pins[0];
    }
    else if (temp_f < 10) {
        mask[3] = (bits[(int) temp_f] | bits[DP]) << display_pins[0];
        mask[2] = bits[(int)(temp_f * 10) % 10] << display_pins[0];
        mask[1] = 0;
    }
    else if (temp_f < 100) {
        mask[3] = bits[(int)(temp_f / 10)] << display_pins[0];
        mask[2] = (bits[(int)temp_f % 10] | bits[DP]) << display_pins[0];
        mask[1] = bits[(int)(temp_f * 10) % 10] << display_pins[0];
    }
    else {
        mask[3] = bits[(int)(temp_f / 100)] << display_pins[0];
        mask[2] = bits[(int)(temp_f / 10) % 10] << display_pins[0];
        mask[1] = bits[(int) temp_f % 10] << display_pins[0];
    }

    for (int i = 0; i < 50 * refresh_seconds; i++) {
        for (int display = 0; display < 4; display++) {
            gpio_put(selector_pins[display], true);
            gpio_set_mask(mask[display]); // set all GPIOs, selected by mask, to high
            sleep_ms(delay);
            gpio_clr_mask(mask[display]); // set all GPIOs, selected by mask, to low
            gpio_put(selector_pins[display], false);
        }
    }
}

void print_humidity(float hum) {
    int32_t mask[4];
    mask[0] = bits[H] << display_pins[0];

    if (hum < 10) {
        mask[3] = (bits[(int) hum] | bits[DP]) << display_pins[0];
        mask[2] = bits[(int)(hum * 10) % 10] << display_pins[0];
        mask[1] = 0;
    }
    else if (hum < 100) {
        mask[3] = bits[(int)(hum / 10)] << display_pins[0];
        mask[2] = (bits[(int)hum % 10] | bits[DP]) << display_pins[0];
        mask[1] = bits[(int)(hum * 10) % 10] << display_pins[0];
    }
    else {
        mask[3] = bits[1];
        mask[2] = mask[1] = bits[0];
    }

    for (int i = 0; i < 50 * refresh_seconds; i++) {
        for (int display = 0; display < 4; display++) {
            gpio_put(selector_pins[display], true);
            gpio_set_mask(mask[display]); // set all GPIOs, selected by mask, to high
            sleep_ms(delay);
            gpio_clr_mask(mask[display]); // set all GPIOs, selected by mask, to low
            gpio_put(selector_pins[display], false);
        }
    }
}