#include <stdio.h>
#include "pico/stdlib.h"
#include "dht.h"

const uint MAX_TIMINGS = 85;

void read_from_dht(dht_reading *result, uint DHT_PIN) {
    int data[5] = {0, 0, 0, 0, 0};
    uint last = 1;
    uint j = 0;

    // send a 0 to DHT to begin transmission
    gpio_set_dir(DHT_PIN, GPIO_OUT);
    gpio_put(DHT_PIN, 0);
    sleep_ms(20);
    gpio_set_dir(DHT_PIN, GPIO_IN);

    gpio_put(LED_PIN, 1);

    for (uint i = 0; i < MAX_TIMINGS; i++) {
        uint count = 0;
        while (gpio_get(DHT_PIN) == last) {
            count++;
            busy_wait_us_32(1);
            if (count == 255) break;
        }
        last = gpio_get(DHT_PIN);
        if (count == 255) break;

        if ((i >= 4) && (i % 2 == 0)) {
            data[j / 8] <<= 1;
            if (count > 50) data[j / 8] |= 1; // if you get "Bad data", try changing 50 to some other value
            j++;
        }
    }

    gpio_put(LED_PIN, 0);

    if ((j >= 40) && (data[4] == ((data[0] + data[1] + data[2] + data[3]) & 0xFF))) {
        result->humidity = int_to_float(data[0], data[1]);
        result->temp_celsius = int_to_float(data[2], data[3]);
    } else {
        printf("Bad data\n");
    }
}

// return a float from two integers representing whole and decimal parts
// if decimal part is negative, it will be converted to positive
float int_to_float(int whole, int decimal) {
    float result = 0;

    if(decimal < 0) {
        decimal = -decimal;
    }

    while(decimal > 0) {
        result += decimal % 10;
        decimal /= 10;
        result /= 10;
    }

    return result + whole;
}