#define LED_PIN PICO_DEFAULT_LED_PIN

typedef struct {
    float humidity;
    float temp_celsius;
} dht_reading;

void read_from_dht(dht_reading *result, uint DHT_PIN);
float int_to_float(int whole, int decimal);