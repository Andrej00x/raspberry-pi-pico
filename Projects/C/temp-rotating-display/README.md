# External temperature and humidity sensor with button and 4x7-seg display

After every measurement, print temperature and humidity to 4x7-segment display.
Rotate displayed characters and cycle between rotation speeds with a button press.
Turn on integrated LED during DHT transmission.