// after measuring, display temperature and humidity on 4x7 segment display
// cycle between 3 rotation speeds with a button press

#include <stdlib.h>
#include "pico/stdlib.h"
#include "dht.h"

enum characters {C = 10, F, H, MINUS, DP, NUL}; // used for easier printing to display
enum speed {SLOW = 40, MEDIUM = 25, FAST = 15}; // how long should display be held until it rotates
                                                // minimum must be 6 to get an accurate DHT reading

float celsius_to_fahrenheit(float temperature);
void print_reading(dht_reading* reading);
void change_pause();

const uint delay = 5; // how long (in ms) should a specific display be turned on (shoudn't change this value)
volatile enum speed curr_speed = SLOW;

const uint DHT_PIN = 18;
const uint BUTTON_PIN = 3;
uint32_t time; // used for debouncing

// segments A, B, ..., G, DP
const uint display_pins [8] = {
    6, 7, 8, 9, 10, 11, 12, 13
};

// displays D1, D2, D3, D4
const uint selector_pins [4] = {
    14, 15, 16, 17
};

int bits[16] = {
        0x3f,  // 0
        0x06,  // 1
        0x5b,  // 2
        0x4f,  // 3
        0x66,  // 4
        0x6d,  // 5
        0x7d,  // 6
        0x07,  // 7
        0x7f,  // 8
        0x6f,  // 9
        0x39,  // C
        0x71,  // F
        0x76,  // H
        0x40,  // -
        0x80,  // DP
        0x00   // NULL
};

int main() {
    gpio_init(DHT_PIN);

    gpio_init(LED_PIN); // LED_PIN defined in dht.h
    gpio_set_dir(LED_PIN, GPIO_OUT);

    gpio_pull_up(BUTTON_PIN);
    gpio_set_irq_enabled_with_callback(BUTTON_PIN, GPIO_IRQ_EDGE_FALL, true, &change_pause);

    // initialize 7-segment display
    for (int i = 0; i < 8; i++) {
        gpio_init(display_pins[i]);
        gpio_set_dir(display_pins[i], GPIO_OUT);
    }
    for (int i = 0; i < 4; i++) {
        gpio_init(selector_pins[i]);
        gpio_set_dir(selector_pins[i], GPIO_OUT);
        gpio_set_outover(selector_pins[i], GPIO_OVERRIDE_INVERT);
    }

    // wait until DHT is ready
    sleep_ms(2000);

    while (true) {
        dht_reading reading;
        // For them to be accurate, readings should be at least 2 seconds apart
        // That interval is done in print_reading function and its minimum (default) value is 6 seconds
        read_from_dht(&reading, DHT_PIN);

        print_reading(&reading);
    }

    return 0;
}

float celsius_to_fahrenheit(float temperature) {
    return (temperature * 9 / 5 + 32);
}

void print_reading(dht_reading* reading) {
    int32_t mask[3+4+2+4+2+4+4];
    float cel, fah, hum;

    cel = reading->temp_celsius;
    fah = celsius_to_fahrenheit(cel);
    hum = reading->humidity;

    for(int i = 0; i < 23; i++) {
        mask[i] = bits[NUL];
    }

    mask[6] = bits[C] << display_pins[0];
    mask[12] = bits[F] << display_pins[0];
    mask[18] = bits[H] << display_pins[0];
    
    // determine celsius display
    if (cel <= -10) {
        mask[3] = bits[MINUS] << display_pins[0];
        mask[4] = bits[(int)(-cel) / 10] << display_pins[0];
        mask[5] = bits[(int)(-cel) % 10] << display_pins[0];
    }
    else if (cel < 0) {
        mask[3] = bits[MINUS] << display_pins[0];
        mask[4] = (bits[(int)(-cel)] | bits[DP]) << display_pins[0];
        mask[5] = bits[(int)((-cel) * 10) % 10] << display_pins[0];
    }
    else if (cel < 10) {
        mask[3] = (bits[(int) cel] | bits[DP]) << display_pins[0];
        mask[4] = bits[(int)(cel * 10) % 10] << display_pins[0];
        mask[5] = 0;
    }
    else {
        mask[3] = bits[(int)(cel / 10)] << display_pins[0];
        mask[4] = (bits[(int)cel % 10] | bits[DP]) << display_pins[0];
        mask[5] = bits[(int)(cel * 10) % 10] << display_pins[0];
    }

    // determine fahrenheit display
    if (fah <= -10) {
        mask[9] = bits[MINUS] << display_pins[0];
        mask[10] = bits[(int)(-fah) / 10] << display_pins[0];
        mask[11] = bits[(int)(-fah) % 10] << display_pins[0];
    }
    else if (fah < 0) {
        mask[9] = bits[MINUS] << display_pins[0];
        mask[10] = (bits[(int)(-fah)] | bits[DP]) << display_pins[0];
        mask[11] = bits[(int)((-fah) * 10) % 10] << display_pins[0];
    }
    else if (fah < 10) {
        mask[9] = (bits[(int) fah] | bits[DP]) << display_pins[0];
        mask[10] = bits[(int)(fah * 10) % 10] << display_pins[0];
        mask[11] = 0;
    }
    else if (fah < 100) {
        mask[9] = bits[(int)(fah / 10)] << display_pins[0];
        mask[10] = (bits[(int)fah % 10] | bits[DP]) << display_pins[0];
        mask[11] = bits[(int)(fah * 10) % 10] << display_pins[0];
    }
    else {
        mask[9] = bits[(int)(fah / 100)] << display_pins[0];
        mask[10] = bits[(int)(fah / 10) % 10] << display_pins[0];
        mask[11] = bits[(int) fah % 10] << display_pins[0];
    }

    //determine humidity display
    if (hum < 10) {
        mask[15] = (bits[(int) hum] | bits[DP]) << display_pins[0];
        mask[16] = bits[(int)(hum * 10) % 10] << display_pins[0];
        mask[17] = 0;
    }
    else if (hum < 100) {
        mask[15] = bits[(int)(hum / 10)] << display_pins[0];
        mask[16] = (bits[(int)hum % 10] | bits[DP]) << display_pins[0];
        mask[17] = bits[(int)(hum * 10) % 10] << display_pins[0];
    }
    else {
        mask[15] = bits[1];
        mask[16] = mask[17] = bits[0];
    }

    for (int mask_it = 0; mask_it < 20; mask_it++) {
        for (int del = 0; del < curr_speed; del++) {
            for (int display = 3; display >= 0; display--) {
                gpio_put(selector_pins[display], true);
                gpio_set_mask(mask[3 - display + mask_it]); // set all GPIOs, selected by mask, to high
                sleep_ms(delay);
                gpio_clr_mask(mask[3 - display + mask_it]); // set all GPIOs, selected by mask, to low
                gpio_put(selector_pins[display], false);
            }
        }
    }
}

void change_pause() {
    const uint32_t debounce = 200;

    if ((to_ms_since_boot(get_absolute_time()) - time) > debounce) {
        time = to_ms_since_boot(get_absolute_time());

        switch(curr_speed) {
            case SLOW:
                curr_speed = MEDIUM;
                break;
            case MEDIUM:
                curr_speed = FAST;
                break;
            case FAST:
            default:
                curr_speed = SLOW;
                break;
        }
    }
}