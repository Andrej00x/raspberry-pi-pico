# External temperature and humidity sensor with RGB LED

Every two seconds measure humidity and temperature with DHT11 sensor (should also work with DHT22).
Set the LED to specific colour based on the temperature.
Turn on integrated LED during DHT transmission.

*I created this project mainly to try programming with my own libraries (basically it's* **ext-temp-sens** *and* **rgb-diode** *projects combined).*