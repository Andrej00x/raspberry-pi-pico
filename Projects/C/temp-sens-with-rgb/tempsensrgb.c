#include <stdio.h>
#include "pico/stdlib.h"
#include "rgb_led.h"
#include "dht.h"

float celsius_to_fahrenheit(float temperature);

const uint DHT_PIN = 16;
const uint MEASURE_PERIOD_MS = 2000; // this must be at least 2000 to get an accurate reading

// measure temperature and humidity every 2 seconds
// set an RGB LED to specific colour depending on the temperature
int main() {
    rgb_led led;
    led.RED_PIN = 0;
    led.GREEN_PIN = 1;
    led.BLUE_PIN = 2;

    stdio_init_all();
    gpio_init(DHT_PIN);
    gpio_init(LED_PIN); // LED_PIN defined in dht.h
    gpio_set_dir(LED_PIN, GPIO_OUT);
    gpio_set_function(led.RED_PIN, GPIO_FUNC_PWM);
    gpio_set_function(led.GREEN_PIN, GPIO_FUNC_PWM);
    gpio_set_function(led.BLUE_PIN, GPIO_FUNC_PWM);

    while (true) {
        dht_reading reading;
        read_from_dht(&reading, DHT_PIN);

        // exact colour values may vary depending on your LED and resistors
        if (reading.temp_celsius <= 5) rgb_set_colour(0, 0, 127, led); // blue
        else if (reading.temp_celsius <= 15) rgb_set_colour(0, 127, 127, led); // cyan
        else if (reading.temp_celsius <= 25) rgb_set_colour(0, 127, 0, led); // green
        else if (reading.temp_celsius <= 30) rgb_set_colour(127, 127, 0, led); // yellow
        else rgb_set_colour(127, 0, 0, led); // red

        printf("Humidity = %.1f%%, Temperature = %.1f C (%.1f F)\n",
               reading.humidity, reading.temp_celsius, celsius_to_fahrenheit(reading.temp_celsius));

        sleep_ms(MEASURE_PERIOD_MS);
    }

    return 0;
}

float celsius_to_fahrenheit(float temperature) {
    return (temperature * 9 / 5 + 32);
}