# Raspberry pi pico

All of my raspberry pi pico projects I've done in my free time.

# Usage

Each project contains 4 files:
* Source code (*.c file)
* pico-sdk-import.cmake  (this one is exactly the same within all projects)
* CMakeLists.txt
* Executable code *.uf2

First three are necessary to compile a code as explained in the 
[official documentation](https://datasheets.raspberrypi.com/pico/getting-started-with-pico.pdf).
Last file is the one you put in your pico board.

# License

This project is released into the public domain. Meaning everyone is free to use, modify, publish, compile etc. 
this software for any means, albeit at their own risk. Some parts of the code are taken from the 
[official Raspberry pi pico examples github](https://github.com/raspberrypi/pico-examples).

# Future

I will post new projects when I make them in my free time, so don't expect any regular uploads. 
I'm a big fan of C language so I probably won't do any project in Python (although that's a possibility).

## Notes

If any of my projects help at least one person, my job is completed.